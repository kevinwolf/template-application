#ApogeeLive Application Template
This is the template for creating a new Application.

##Developing
1. Remove the git repository, this is because we don't want to keep the commits of this template in the commit history of the App.
2. Run `npm install`.
3. Create a new feature branch of the feature you will be working on related to the Jira ticket. (For example `feature/AL-101-create-a-new-page`).
4. Push the feature branch to Bitbucket and **create a pull request**. Add other developers as a reviewers and **wait them to approve the changes**.
5. Once the developers approved the changes, merge the feature branch with `develop`.

##Deploying
Deploys happen automatically when pushing to `develop` or `master`.

###Staging URL
*TBD*

###Production URL
*TBD*
