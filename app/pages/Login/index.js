import React, { Component } from 'react';
import { Link } from 'react-router';
import { RaisedButton } from 'material-ui';
import style from './style.scss';

export default class Login extends Component {

  render () {
    return <div className="login">
      <h1>Login page</h1>
      <Link to="home">
        <RaisedButton label="Login" />
      </Link>
    </div>;
  }

}
