import React, { Component } from 'react';
import ApogeeLogo from '../../components/ApogeeLogo';

export default class Home extends Component {

  render () {
    return <div>
      <h1>Home page.</h1>
      <ApogeeLogo />
    </div>;
  }

}
