import React from 'react';
import Router from 'react-router';
import { Route, DefaultRoute } from 'react-router';

// Layouts.
import Root from './layouts/Root';
import App from './layouts/App';

// Pages.
import Login from './pages/Login';
import Home from './pages/Home';
import About from './pages/About';

// Routes definition.
const Routes = (
  <Route name="root" path="/" handler={Root}>

    <Route name="app" handler={App}>
      <Route name="home" handler={Home} />
      <Route name="about" handler={About} />
      <DefaultRoute handler={Home} />
    </Route>

    <Route name="login" handler={Login} />
    <DefaultRoute handler={Login} />
  </Route>
);

export default Routes;
