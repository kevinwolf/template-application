import React from 'react';
import { Link, RouteHandler } from 'react-router';
import { AppBar, LeftNav, FlatButton } from 'material-ui';

const menuItems = [
  { route : 'home', text : 'Home page' },
  { route : 'about', text : 'About page' }
];

export default class App extends React.Component {

  static contextTypes = {
    router : React.PropTypes.func
  }

  render () {
    return <div>
      <AppBar
        title="Application Title"
        iconElementRight={<FlatButton label="Log out" onClick={this._logOut} />}
        onLeftIconButtonTouchTap={this._onLeftIconButtonTouchTap}
        zDepth={0} />

      <LeftNav
        ref="appNavbar"
        docked={false}
        menuItems={menuItems}
        selectedIndex={this._getSelectedIndex()}
        onChange={this._onLeftNavChange} />

      <RouteHandler />
    </div>;
  }

  // Toggle Side Bar.
  _onLeftIconButtonTouchTap = () => {
    this.refs.appNavbar.toggle();
  }

  // Log Out.
  _logOut = () => {
    this.context.router.transitionTo('login');
  }

  // Get actual link and make it selected on the Side Bar.
  _getSelectedIndex = () => {

    for (let i in menuItems) {
      if (this.context.router.isActive(menuItems[i].route)) return parseInt(i);
    }
  }

  // Navigate to route when clicking on a Side Bar element.
  _onLeftNavChange = (e, key, payload) => {
    this.context.router.transitionTo(payload.route);
  }

}
